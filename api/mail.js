import nodemailer from 'nodemailer';

const auth = {
  host: "smtp.gmail.com",
     port: 465,
     secure: true, // true for 465, false for other ports
     auth: {
       user: 'romainelouard@gmail.com', // generated ethereal user
       pass: '03067rom', // generated ethereal password
     },
}
// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport(auth)

/**
 * Fonction asynchrone pour envoyer le mail.
 * @param req va comprendre les informations de la requête.
 * @param res Va comprendre les informations de la réponse.
 */
async function sendMail (req, res) {
  if (req.method !== 'POST') {
     res.statusCode = 405
     res.end('Method not allowed')
  }
  let mailOptions = {
    from: 'Flora\'fit Healthy Life <service.contact@flora-juice.com>', // sender address
    to: "romainelouard33@hotmail.com", // list of receivers
  }
  // setup e-mail data with unicode symbols
  if (req.body.typeMail === 'Date') {
      mailOptions.subject = `Demande de rendez-vous pour ${req.body.name}`, // Subject line
      mailOptions.text = `Demande de rendez-vous pour ${req.body.name}`, // plain text body
      mailOptions.html = `<h1>Une nouvelle demande de rendez-vous vient d'être demandé par ${req.body.name}</h1><p>Voici les informations que le contact a rempli :</p><ul><li>Nom : ${req.body.name}</li><li>Date : ${req.body.date}</li><li>Contact : ${req.body.contact} (${req.body.contactInformation})</li></ul>` // html body
  } else {
    mailOptions.subject = `Nouveau bilan pour ${req.body.name}`, // Subject line
    mailOptions.text = `Nouveau bilan pour ${req.body.name}`, // plain text body
    mailOptions.html = `<h1>Un nouveau bilan vient d'être demandé par ${req.body.name}</h1><p>Voici les informations que le contact a rempli :</p><ul><li>Ville : ${req.body.city}</li><li>Poids, taille, âge : ${req.body.bodyInformation}</li><li>Objectif physique : ${req.body.target}</li><li>Sport : ${req.body.sport} (${req.body.sportPractise})</li><li>Repas habituels : ${req.body.eat} (${req.body.meal === 'Autre :' ? req.body.meal + ' ' + req.body.otherMeal : req.body.meal })</li><li>Grignotage : ${req.body.snack}</li><li>Nombre de repas avec poisson : ${req.body.fish}</li><li>Fruits et légumes : ${req.body.fruit}</li><li>Régime particulier : ${req.body.eatParticularity}</li><li>Energie journalière : ${req.body.energy}</li><li>Nombre d'heure par nuit : ${req.body.night}</li><li>Particularité de la peau : ${req.body.skin}</li><li>Allergies, intolérances, problèmes de santé : ${req.body.healthProblems}</li><li>Douleurs : ${req.body.pain}</li><li>Médicaments : ${req.body.pills}</li><li>Contact : ${req.body.contact === 'Autre :' ? req.body.contact + ' ' + req.body.otherContact : req.body.contact } (${req.body.contactInformation})</li></ul>` // html body
  }
  // send mail with defined transport object
  try {
    await transporter.sendMail(mailOptions)
    res.end('Mail sended')
  } catch (e) {
    console.log('error', e);
    res.statusCode = 500
    res.end('Error occured when sending mail')
  }
}

module.exports = sendMail
